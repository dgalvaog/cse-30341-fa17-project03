// client.h: PS Client Library -------------------------------------------------
#pragma once

#include <string>
#include <mutex>
#include <map>

#include "callback.h"
#include "thread.h"
#include "queue.h"

class Client {
    public:
        Client(const char *host, const char *port, const char *cid);
        void publish(const char *topic, const char *message, size_t length);
        void subscribe(const char *topic, Callback *callback);
        void unsubscribe(const char *topic);
        void disconnect();
        void run();
        bool shutdown();

        Queue<Message>* getOutputQueue();
        Queue<Message>* getRetrievedQueue();
        const char * getHost();
        const char * getPort();
        const char * getCid();
        size_t getNonce();

        void setRunning(bool);

        void callCallback(Message m);

    private:
    	const char *host;
    	const char *port;
    	const char *cid;
    	size_t nonce;

        Callback *callback;

    	Thread callback_t;
    	Thread send_t;
    	Thread retrieve_t;

    	Queue<Message> outputQueue;
    	Queue<Message> retrievedQueue;

        std::mutex lock;    // will be used to lock access to boolean 'running', as it is a shared resource across threads
        bool running;       // will be used to signify if client is still running
};

// vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: ----------------------------------
