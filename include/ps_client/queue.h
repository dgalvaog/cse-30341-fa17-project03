#pragma once

#include "macros.h"

#include <queue>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>

const size_t QUEUE_MAX = 64;

template <typename T>
class Queue {
private:
    std::queue<T>   data;
    T           sentinel;
    pthread_mutex_t lock;
    pthread_cond_t  okToPop;
    pthread_cond_t  okToPush;

public:
    Queue(){
        sentinel = (Message){"","","",(size_t)(-1),""};
        int rc;
        Pthread_mutex_init(&lock, NULL);
        Pthread_cond_init(&okToPop, NULL);
        Pthread_cond_init(&okToPush, NULL);
    }

    void push(const T &value) {
        int rc;
        
        Pthread_mutex_lock(&lock);                  // lock critical section
        while (data.size() >= QUEUE_MAX) {
            Pthread_cond_wait(&okToPush, &lock);    // is queue is full, wait for something to popped off
        }

        data.push(value);
        Pthread_cond_signal(&okToPop);             // push something to queue, and signal okToPop because the queue is not empty if it was before
        Pthread_mutex_unlock(&lock);
    }
    
    T pop() {
        int rc;

        Pthread_mutex_lock(&lock);                  // lock critical section
        while (data.empty()) {
            Pthread_cond_wait(&okToPop, &lock);     // if queue is empty, wait for okToPop cond variable to be signaled
        }

        T value = data.front();
        if (value != sentinel) {
            data.pop();
            Pthread_cond_signal(&okToPush);         // if value is not sentinel, pop it and signal okToPush
        } else {
            Pthread_cond_signal(&okToPop);          // if value is sentinel, don't pop it off and signal okToPop
        }
        Pthread_cond_signal(&okToPush);
        Pthread_mutex_unlock(&lock);
        return value;
    }

    int size(){ return data.size(); }
};
