#pragma once

#include <iostream>

struct Message{
	std::string type;        // Message type (MESSAGE, IDENTIFY, SUBSCRIBE, UNSUBSCRIBE, RETRIEVE, DISCONNECT)
	std::string topic;       // Message topic
	std::string sender;      // Message sender
	size_t      nonce;       // Sender's nonce
	std::string body;        // Message body	

	bool operator==(const Message& m) const{
	    return m.nonce == nonce;
	}
	
	bool operator!=(const Message& m) const{
	    return !(m.nonce == nonce);
	}
};

class Callback{
	public:
		virtual void run(Message &m) = 0;	
};

class EchoCallback : public Callback {
public:
    void run(Message &m) {
        std::cout << m.body << std::endl;
    }
};

class BoardCallback : public Callback {
public:
	void run(Message &m){
            int idx = 0;
            for(unsigned int i=0; i<m.body.size(); i++)
            {
                if(m.body[i] == ':'){
                    idx = i;
                    break;
                }
            }
	    std::cout << "NEW message from " << m.body.substr(idx+1) << ": " << m.body.substr(0,idx)  << std::endl;
	}
};
