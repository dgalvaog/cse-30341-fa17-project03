#pragma once

#include "client.h"
#include <fstream>
#include <poll.h>

void *dequeueOutputQueue(void *arg);
void *enqueueMsgsRetrieved(void *arg);
void *callCallbacks(void *arg);

void connectToServer(int &, FILE* &, const char*);
std::string sendMessageToServer(Message, int, FILE* &, struct pollfd);
const char * generateStringFromMessage(Message);
