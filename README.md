CSE.30341.FA17: Project 03
==========================

This is the documentation for [Project 03] of [CSE.30341.FA17].

Members
-------

1. Daniel Galvao Guerra (dgalvaog@nd.edu)
2. James Holland (jhollan4@nd.edu)

Design
------

> 1. The client library needs to provide a `Client` class.
>
>   - What does the `Client` class need to keep track of?
>
>   - How will the `Client` connect to the server?
>
>   - How will it implement concurrent publishing, retrieval, and processing of
>     messages?
>
>   - How will it pass messages between different threads?
>
>   - What data members need to have access synchronized? What primitives will
>     you use to implement these?
>
>   - How will threads know when to block or to quit?
>
>   - How will the `Client` determine which `Callbacks` belong to which topics?

The client will connect to the server via network sockets, and for this it will need to keep track of the server host, port, and it will also need
to store the client indentifier so the server can store publishing and subscription information for that specific client.
Concurrent publishing, retrieval, and processing of messages will be done with 3 different threads: main, publish, and subscribe. Messages can be passed
between threads with a shared buffer (one between main and publish, and one between main and subscribe). The data that will need to be synchronized will
be everything that keeps track of each clients' subscription information, as well as information about the client themselves, because both the subscribe
and publish threads will need that information, and the main thread will probably store it. To synchronize data access we will either use semaphores or
just the mutex lock and conditional variable primitives.

The threads will know when to block and quit based on the command being issued and the condition variables. 

When you call Client.subscribe you pass in a topic and a Callback and so it can keep track of that pair.

> 2. The client library needs to provide a `Callback` class.
>
>   - What does the `Callback` class need to keep track of?
>
>   - How will applications use this `Callback` class?

The callback class needs to keep track of the type of command that is that client is issuing, as the function that is run is dependent on the type of command. 

Applications will indirectly use the callback class by sending messages to the server that will run specific functions on the server. 

> 3. The client library needs to provide a `Message` struct.
>
>   - What does the `Message` struct need to keep track of?
>
>   - What methods would be useful to have in processing `Messages`?

The message struct needs to keep track of the message type, the message topic, the message sender, the message nonce, and the message body.

The message type can be IDENTIFY, SUSCRIBE, UNSUBSCRIBE, PUBLISH, RETRIEVE, DISCONNECT.

The publish method in the client class will take a message and publish it to the server. Additionally, the run method in the client server will process any incoming messages. 

> 4. The client library needs to provide a `Thread` class.
>
>   - What does the `Thread` class need to keep track of?
>
>   - What POSIX thread functions will need to utilize?

The tread class should record which type of operation it is running (publishing, retreiving, or callback) and the arguments that are being passed. 

Functions POSIX threads need to utilize include Pthread_join, Pthread_exit,Pthread_create, Pthread_mutex_lock, Pthread_mutex_unlock, as well as condition variables such as Pthread_cond_wait and Pthread_cond_signal. 

> 5. You will need to perform testing on your client library.
>
>   - How will you test your client library?
>
>   - What will `echo_test` tell you?
>
>   - How will you use the Google Test framework?
>
>   - How will you incorporate testing at every stage of development?

We will test the client library by writing scripts that create clients, connect them to the server, and perform a variety of different client commands. We will then check the output to make sure that the program performed as planned. 

Echo test generates a number of messages and then tell us if they are being handled correctly. 

We can use the google test framework to quickly write a number of tests in a well-organized framework. According to the google framework documentation, it should also help us get more detailed error reports, reused shared resources, and wil continue running tests even after one of the individual tests fails. 

We will incorporate testing at every stage by periodically writing test cases that represent correct outputs for specific parts of the program, testing them individually, and then continuing to confirm that the program is working correctly as we put the individual parts together. 

> 6. You will need to create an user application that takes advantage of the
>    pub/sub system.
>
>   - What distributed and parallel application do you plan on building?
>
>   - How will utilize the client library?
>
>   - What topics will you need?
>
>   - What callbacks will you need?
>
>   - What additional threads will you need?

We plan on building a message board that allows users to post messages to certain topics based on their interests. Multiple 
different users will be able to post to a topic at the same time and receive each other's messages. When a message is displayed, 
the user's id will be displayed too.

We will utilize the client library to create clients, contact/identify with the server, subscribe to topics, retrieve messages 
from those subscribed topics, and disconnect from the server. 

The topics needed will be dependent on the user, who will specify either an existing topic, or create a new one. 

We will need a callback to display the messages sent to the server along with the name of the user who sent it. 

Each user starts a different client, so multiple threads from different clients will be in contact with the server. 
We will not need additional threads in a single application to make use of the distributed nature of the application. 

Demonstration
-------------

https://docs.google.com/a/nd.edu/presentation/d/13fYCoI3xipFBrgvqa0y-spFFRkD86-pxNcA5e3AufdE/edit?usp=sharing

Errata
------

> Valgrind exhibits possibly lost and still reachable blocks. Server sometimes throws "ERROR Handle failure: _Map_base::at" errors after cleaning up clients - I could not figure out why. Doesn't always happen.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 03]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project03.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
