#include <string>
#include <unistd.h>
#include <cstdlib>
#include <iostream>

#include "../../include/ps_client/client.h"
#include "../../include/ps_client/macros.h"
#include "../../include/ps_client/queue.h"
#include "../../include/ps_client/thread.h"
#include "../../include/ps_client/util_funcs.h"

using namespace std;

//This is an application that allows the user to treat the server like an online message board

void * access_board(void * clientArgument);

void displayUsage(){
	cout << "Usage: ./application host port cid" << endl;
	exit(1);

} 

int main(int argc, char ** argv){

	if(argc != 4){
		displayUsage();
		exit(1);
	}	

 	char * host_input = argv[1];
	char * host_port = argv[2];
	char * host_cid = argv[3];

	//callback
	Client localClient(host_input, host_port, host_cid);
	Thread newThread;

	newThread.start(access_board, (void *)&localClient);
	newThread.join(NULL);

	return 1;
}

void  * access_board(void * clientArgument){

	Client * localClient = (Client *)clientArgument;

	string topicName;

	cout << "Welcome " << localClient->getCid() << endl;
        cout << "Chose a message board to enter: ";
        cin >> topicName;

        BoardCallback e;
	localClient->subscribe(topicName.c_str(), &e);
	localClient->run();

        cout << "Type a message to post to the message board, as messages are posted they will appear here." << endl;
	cin.ignore();
        for(string message; getline(cin, message);){
            string body = message+":"+localClient->getCid();
            localClient->publish(topicName.c_str(), body.c_str(), sizeof(message));
        }

	localClient->disconnect();
	return NULL;	
}
	

	
