// SYSTEM INCLUDES

#include <gtest/gtest.h>
#include <string>
#include "../../include/ps_client/client.h"
#include "../../include/ps_client/macros.h"
#include "../../include/ps_client/queue.h"
#include "../../include/ps_client/thread.h"
#include "../../include/ps_client/util_funcs.h"
#include "../../include/ps_client/callback.h"

using namespace std;
// C++ PROJECT INCLUDES

//TEST_F

class ClientTest : public ::testing::Test{
	protected:
	virtual void SetUp(){
	}

	virtual void TearDown(){
	}
};

class MessageTest : public ::testing::Test{
	protected:
	virtual void SetUp(){
	}

	virtual void TearDown(){
	}
};

class CallbackTest : public ::testing::Test{
	protected:
	virtual void SetUp(){
	}

	virtual void TearDown(){
	}
};

class ThreadTest : public ::testing::Test{
	protected:
	virtual void SetUp(){
	}

	virtual void TearDown(){
	}
};

class OutputQueueTest : public ::testing::Test{
	protected:
	virtual void SetUp(){
	}

	virtual void TearDown(){
	}
};
class RetrievedQueueTest : public ::testing::Test{
	protected:
	virtual void SetUp(){
	}

	virtual void TearDown(){
	}
};


TEST_F(ClientTest, Client){
	string host = "host";
	string port = "8080";
	string cid = "123";

	Client testClient(host.c_str(), port.c_str(), cid.c_str());
	ASSERT_TRUE(&testClient != NULL);

	string topic = "Boats";
	string message = "Boats are really cool";
	size_t length = sizeof(message);

	testClient.publish(topic.c_str(), message.c_str(), length);
	ASSERT_TRUE(testClient.getOutputQueue()->size() > 0);	
		
	int currentSize = testClient.getOutputQueue()->size();
	EchoCallback * callback = {};
	testClient.subscribe(topic.c_str(), callback);
	ASSERT_TRUE(testClient.getOutputQueue()->size() > currentSize);	

	currentSize = testClient.getOutputQueue()->size();
	testClient.unsubscribe(topic.c_str());
	ASSERT_TRUE(testClient.getOutputQueue()->size() > currentSize);	

	currentSize = testClient.getOutputQueue()->size();
	testClient.disconnect();
	ASSERT_TRUE(testClient.getOutputQueue()->size() > currentSize);	

}

TEST_F(MessageTest, Message){
	size_t nonce = 111;	
	Message msg = (Message){"IDENTIFY", "small houses", "cid",nonce,"small houses are small"};
	ASSERT_TRUE(&msg != NULL);
	ASSERT_TRUE(msg.type == "IDENTIFY");
	ASSERT_TRUE(msg.topic == "small houses");
	ASSERT_TRUE(msg.sender == "cid");
	ASSERT_TRUE(msg.nonce == nonce);
	ASSERT_TRUE(msg.body == "small houses are small");
}

TEST_F(CallbackTest, Callback){
	EchoCallback call;
	ASSERT_TRUE(&call != NULL);
}	

TEST_F(ThreadTest, Thread){
	Thread testThread;
	ASSERT_TRUE(&testThread != NULL);
}

TEST_F(OutputQueueTest, OutputQueue){
	string host = "host";
	string port = "8080";
	string cid = "123";
	string topic = "parkour";
	string message = "wow...fun";
	size_t length = sizeof(message);

	Client testClient(host.c_str(), port.c_str(), cid.c_str());
	
	int size = testClient.getOutputQueue()->size();
	testClient.unsubscribe(topic.c_str());
	ASSERT_TRUE(testClient.getOutputQueue()->size() > size);

	size = testClient.getOutputQueue()->size();
	testClient.publish(topic.c_str(), message.c_str(),length);
	ASSERT_TRUE(testClient.getOutputQueue()->size() > size);
	
}

TEST_F(RetrievedQueueTest, RetrievedQueue){
	string host = "host";
	string port = "8080";
	string cid = "123";
	
	Client testClient(host.c_str(), port.c_str(), cid.c_str());
	Queue<Message> * testQueue = testClient.getRetrievedQueue();
	ASSERT_TRUE(testQueue != NULL);		
}	
int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
