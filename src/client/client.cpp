#include <iostream>

#include "../../include/ps_client/client.h"
#include "../../include/ps_client/util_funcs.h"

Client::Client(const char *host, const char *port, const char *cid){
	this->host = host;
	this->port = port;
	this->cid = cid;
	srand(time(NULL));
    this->nonce = (size_t)(rand()%1000);
}

void Client::publish(const char *topic, const char *message, size_t length){
	Message msg = {"PUBLISH", topic, cid, nonce, message};
        outputQueue.push(msg);
}

void Client::subscribe(const char *topic, Callback *callback){
	Message msg = {"SUBSCRIBE", topic, cid, nonce, ""};
	outputQueue.push(msg);

	this->callback = callback;
}

void Client::unsubscribe(const char *topic){	
	Message msg = {"UNSUBSCRIBE", topic, cid, nonce, ""};
	outputQueue.push(msg);
}

void Client::disconnect(){
    Message msg = {"DISCONNECT", "", std::string(cid), nonce, ""};
	outputQueue.push(msg);
}

void Client::run(){
	//callback_t, send_t, retrieve_t
	send_t.start(dequeueOutputQueue, (void *)this);
	send_t.detach();
	retrieve_t.start(enqueueMsgsRetrieved, (void *)this);
	retrieve_t.detach();
	callback_t.start(callCallbacks, (void *)this);
	callback_t.detach();	

	running = true;
}

bool Client::shutdown(){

	lock.lock();
	bool tmp = running;
	lock.unlock();

	return !tmp;
}

void Client::setRunning(bool b){
	lock.lock();
	running = b;
	lock.unlock();
}

Queue<Message>* Client::getOutputQueue(){
	return &outputQueue;
}

Queue<Message>* Client::getRetrievedQueue(){
	return &retrievedQueue;
}

void Client::callCallback(Message m){
    callback->run(m);
}

const char * Client::getHost(){ return host; }
const char * Client::getPort(){ return port; }
const char * Client::getCid() { return cid;  }
size_t Client::getNonce()     { return nonce;}

