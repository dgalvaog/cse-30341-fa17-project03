#include "../../include/ps_client/util_funcs.h"
#include "../../include/ps_client/client.h"

#include <cstring>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <errno.h>
#include <sys/un.h>
#include <poll.h>

int globalSocketCounter = 0;

// This thread will dequeue messages and send them to the server
void *dequeueOutputQueue(void *arg){
    Client *client = (Client *)arg;
	
    int client_fd;
    FILE *server_stream;
    connectToServer(client_fd, server_stream, client->getPort());
    struct pollfd pfds = {client_fd, POLLIN|POLLPRI, 0};

    // Identify thread
    Message identifyMsg = (Message){"IDENTIFY","",std::string(client->getCid()),client->getNonce(),""};
    sendMessageToServer(identifyMsg, client_fd, server_stream, pfds);

    Message outputMsg;
    while(1)
    {
	outputMsg = client->getOutputQueue()->pop();
        std::string response = sendMessageToServer(outputMsg, client_fd, server_stream, pfds);

	if(outputMsg.type.compare("DISCONNECT") == 0){
	    break;
	}

	sleep(1);
    }

    client->setRunning(false); 

    fclose(server_stream);
    close(client_fd);

    return NULL;
}

// This thread will continuosly send "RETRIEVE" messages to the server and enqueue the reponse message back as a Message struct
void *enqueueMsgsRetrieved(void *arg){
    Client *client = (Client *)arg;

	int client_fd;
	FILE* server_stream;
	connectToServer(client_fd, server_stream, client->getPort());
    struct pollfd pfds = {client_fd, POLLIN|POLLPRI, 0};

    // Identify thread
    Message identifyMsg = (Message){"IDENTIFY","",std::string(client->getCid()),client->getNonce(),""};
    sendMessageToServer(identifyMsg, client_fd, server_stream, pfds);

    Message recMsg = (Message){"RETRIEVE","",client->getCid(),client->getNonce(),""};

	// connect to server
	while(!client->shutdown())
    {	
    	std::string response = sendMessageToServer(recMsg, client_fd, server_stream, pfds);
	//printf("msg gotten from retrieve call:%s",response.c_str());    
        if(response.size() > 0){
			Message serverResponse;
			serverResponse.type = "MESSAGE";
			serverResponse.sender = client->getCid();
			serverResponse.nonce = client->getNonce();

			int spaceCount = 0;
			int lastSpaceIdx = -1;
			int bodyLen = 0;
			for(unsigned int i=0; i<response.size(); i++){
				if(response[i] == ' '){
					spaceCount++;
					if(spaceCount == 2){
						serverResponse.topic = response.substr(lastSpaceIdx+1, i-lastSpaceIdx-1);
					}
					lastSpaceIdx = i;
				}
				else if(response[i] == '\n'){
					bodyLen = std::atoi(response.substr(lastSpaceIdx+1, i-lastSpaceIdx-1).c_str());
					serverResponse.body = response.substr(i+1, bodyLen); // get body, but truncate at because buffer is pottentially not cleared
				}
			}

			// enqueue response into retrievedQueue as a Message struct
			client->getRetrievedQueue()->push(serverResponse);
		}

		sleep(1);
	}


	fclose(server_stream);
    close(client_fd);

	return NULL;
}

// This thread will continuously dequeue messages in the retrieved queue and call the appropriate callback to handle them
void *callCallbacks(void *arg){
	Client *client = (Client *)arg;

	while(!client->shutdown())
	{
		Message retrievedMsg = client->getRetrievedQueue()->pop();
		client->callCallback(retrievedMsg);
	}

	return NULL;
}

// Helper function, connects to the server
void connectToServer(int &client_fd, FILE* &server_stream, const char* PORT){

    struct sockaddr_in serv_addr;
    if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(PORT));
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        printf("\nInvalid address/ Address not supported \n");
    } 
          
    if (connect(client_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    { 
        printf("\nConnection Failed \n");
    }
    
    server_stream = fdopen(client_fd, "r+");
    if (server_stream == NULL) {
        perror("fdopen");
        exit(EXIT_FAILURE);
    }
}

// Helper function, send a message struct to the server
std::string sendMessageToServer(Message m, int client_fd, FILE* &server_stream, struct pollfd pfds){
    const char *msgToSend = generateStringFromMessage(m);
    send(client_fd, msgToSend, strlen(msgToSend), 0);

    char buffer[BUFSIZ];
    recv(client_fd, buffer, BUFSIZ, 0);

    std::string response(buffer);
    return response;
}

// Helper function, generate the right char* string from a message struct depending on the type
const char * generateStringFromMessage(Message m){
	std::string msg = m.type;

	if(m.type.compare("IDENTIFY") == 0){
		msg += " ";
		msg += m.sender;
		msg += " ";
		msg += std::to_string((int)m.nonce);
        msg += "\n";
	}
	else if(m.type.compare("SUBSCRIBE") == 0){
		msg += " ";
		msg += m.topic;
                msg += "\n";
	}
	else if(m.type.compare("UNSUBSCRIBE") == 0){
		msg += " ";
		msg += m.topic;
                msg += "\n";
	}
	else if(m.type.compare("PUBLISH") == 0){
		msg += " ";
		msg += m.topic;
		msg += " ";
		msg += std::to_string(strlen(m.body.c_str()));
                msg += "\n";
		msg += m.body;
	}
	else if(m.type.compare("RETRIEVE") == 0){
		msg += " ";
		msg += m.sender;
        msg += "\n";
	}
	else if(m.type.compare("DISCONNECT") == 0){
		msg += " ";
		msg += m.sender;
		msg += " ";
		msg += std::to_string((int)m.nonce);
                msg += "\n";
	}

	return msg.c_str();
}
